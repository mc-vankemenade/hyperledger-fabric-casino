package main

import (
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"time"

	"github.com/hyperledger/fabric-chaincode-go/pkg/cid"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type SmartContract struct {
	contractapi.Contract
}

func isAdmin(identity cid.ClientIdentity) bool {
	id, err := identity.GetX509Certificate()
	if err != nil {
		fmt.Printf("something went wrong getting the certificate: %s", err.Error())
	}

	if id.Subject.CommonName == "admin" {
		return true
	} else {
		return false
	}

}

func (s *SmartContract) Init(ctx contractapi.TransactionContextInterface) error {

	identity := ctx.GetClientIdentity()

	if !isAdmin(identity) {
		return fmt.Errorf("only admin can init")
	} else {
		// set starting prize to begin
		ctx.GetStub().PutState("prize", []byte(strconv.Itoa(10)))

		id, _ := ctx.GetClientIdentity().GetID()
		ctx.GetStub().PutState("adminaccount", []byte(id))
		return nil
	}

}

func (s *SmartContract) Guessnum(ctx contractapi.TransactionContextInterface, guess int) map[string]int {

	// mapping for returns
	returnval := make(map[string]int)

	// params for invoke to casino_token
	params := []string{"ClientAccountBalance"}
	queryArgs := make([][]byte, len(params))
	for i, arg := range params {
		queryArgs[i] = []byte(arg)
	}
	// Check balance, you need 5 to participate
	response := ctx.GetStub().InvokeChaincode("casino_token", queryArgs, "mychannel")
	strrep, _ := strconv.Atoi(string(response.Payload))
	if strrep >= 5 {
		adminaccountbytes, _ := ctx.GetStub().GetState("adminaccount")
		adminaccount := string(adminaccountbytes)

		// transfer 5 tokens to admin account
		params := []string{"Transfer", string(adminaccount), "5"}
		queryArgs := make([][]byte, len(params))
		for i, arg := range params {
			queryArgs[i] = []byte(arg)
		}
		ctx.GetStub().InvokeChaincode("casino_token", queryArgs, "mychannel")

		// get timemstamp from STUB
		timestamp, _ := ctx.GetStub().GetTxTimestamp()
		rand.Seed(time.Unix(timestamp.Seconds, int64(timestamp.Nanos)).UnixNano())
		min := 1
		max := 5
		randint := rand.Intn(max-min+1) + min
		log.Printf("randint: %d", randint)

		if guess == randint {
			log.Println("Correct")

			numbytes, err := ctx.GetStub().GetState("prize")
			var prize int
			if err != nil {
				log.Println(err.Error())
				returnval["error"] = 0
				return returnval
			}
			prize, _ = strconv.Atoi(string(numbytes))
			prize += 5
			returnval["prize"] = prize

			// set prize to default to reset
			ctx.GetStub().PutState("prize", []byte(strconv.Itoa(10)))
			return returnval
		} else {
			// if guess incorrect
			log.Println("not corrrect")

			numbytes, err := ctx.GetStub().GetState("prize")
			var num int
			if err != nil {
				log.Println(err.Error())
				returnval["error"] = 0
				return returnval
			}

			num, _ = strconv.Atoi(string(numbytes))
			prize := num + 5

			ctx.GetStub().PutState("prize", []byte(strconv.Itoa(prize)))
			returnval["false"] = 0
			return returnval
		}
	} else {
		returnval["Not enough funds. Need 5 to participate"] = 0
		return returnval
	}

}

func main() {
	chaincode, err := contractapi.NewChaincode(new(SmartContract))

	if err != nil {
		log.Printf("Error creating chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		log.Printf("Error starting chaincode: %s", err.Error())
	}
}
