package game

type game struct {
	Id string
	Name string
	Desc string
	IsEnabled bool
}

func New(id string, name string, desc string, enabled bool) game {
	g := game{}
	g.Id = id
	g.Name = name
	g.Desc = desc
	g.IsEnabled = enabled

	return g
}

func (g game) GetId() string {
	return g.Id
}

func (g game) GetName() string {
	return g.Name
}

func (g game) GetDesc() string {
	return g.Desc
}

func (g game) GetStatus() bool {
	return g.IsEnabled
}

func (g game) SetName(newName string) {
	g.Name = newName
} 

func (g game) SetDesc(newDesc string) {
	g.Desc = newDesc
} 

func (g game) Enable() {
	g.IsEnabled = true
} 

func (g game) Disable() {
	g.isEnabled = false
} 