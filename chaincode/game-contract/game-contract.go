package main

// Import packages
import (
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"github.com/hyperledger/fabric-chaincode-go/pkg/cid"
	"errors"
	"main/game"
	"encoding/json"
)

// Init Smart-Contract type with the default contract-api properties
type SmartContract struct {
	contractapi.Contract
}

func (s *SmartContract) RegisterGame(ctx contractapi.TransactionContextInterface, id string, name string, desc string, enabled bool) (string) {
	identity := ctx.GetClientIdentity();

	if (!isAdmin(identity)) {
		errors.New("Only Administrators can Register new Games.");
	}

	game := game.New(id, name, desc, enabled);


	bytes, err := json.Marshal(game)

	if (err != nil) {
		fmt.Printf("something went wrong with encoding: %s", err.Error())
	}

	err = ctx.GetStub().PutState(id, bytes);

	if (err != nil) {
		fmt.Printf("something went wrong with writing to the blockchain: %s", err.Error())
	}

	return string(bytes)

}

func (s *SmartContract) GetGame(ctx contractapi.TransactionContextInterface, id string) (string) {
	bytes, err := ctx.GetStub().GetState(id);

	if (err != nil) {
		fmt.Printf("something went wrong with writing to the blockchain: %s", err.Error())
	}

	return string(bytes)

}

func (s *SmartContract) EnableGame(ctx contractapi.TransactionContextInterface, id string) (string) {
	identity := ctx.GetClientIdentity();

	if (!isAdmin(identity)) {
		errors.New("Only Administrators can Register new clients.");
	}

	bytes, err := ctx.GetStub().GetState(id);

	game := game.New("", "", "", false)

	json.Unmarshal(bytes, &game)

	game.Enable()

	bytes, err = json.Marshal(game)

	if (err != nil) {
		fmt.Printf("something went wrong with encoding: %s", err.Error())
	}

	err = ctx.GetStub().PutState(id, bytes);

	return string(bytes)

}

func (s *SmartContract) DisableGame(ctx contractapi.TransactionContextInterface, id string) (string) {
	identity := ctx.GetClientIdentity();

	if (!isAdmin(identity)) {
		errors.New("Only Administrators can Register new clients.");
	}

	bytes, err := ctx.GetStub().GetState(id);

	game := game.New("", "", "", false)

	json.Unmarshal(bytes, &game)

	game.Enable()

	bytes, err = json.Marshal(game)

	if (err != nil) {
		fmt.Printf("something went wrong with encoding: %s", err.Error())
	}

	err = ctx.GetStub().PutState(id, bytes);

	return string(bytes)

}

func isAdmin(identity cid.ClientIdentity) bool {
	id, err := identity.GetX509Certificate()
	if err != nil {
		fmt.Printf("something went wrong getting the certificate: %s", err.Error())
	}

	if id.Subject.CommonName == "admin" {
		return true
	} else {
		return false
	}
}

func main() {

	chaincode, err := contractapi.NewChaincode(new(SmartContract))

	if err != nil {
		fmt.Printf("Error creating chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting chaincode: %s", err.Error())
	}

	fmt.Print("Chaincode create successfully")
}
