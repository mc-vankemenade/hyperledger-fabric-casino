package main

// Import packages
import (
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"github.com/hyperledger/fabric-chaincode-go/pkg/cid"
	"errors"
	"main/participant"
	"encoding/json"
)

// Init Smart-Contract type with the default contract-api properties
type SmartContract struct {
	contractapi.Contract
}

func (s *SmartContract) RegisterClient(ctx contractapi.TransactionContextInterface, id string, name string, role string) (string) {
	identity := ctx.GetClientIdentity();

	if (!isAdmin(identity)) {
		errors.New("Only Administrators can Register new clients.");
	}

	participant := participant.New(id, name, role);


	bytes, err := json.Marshal(participant)

	if (err != nil) {
		return string(err.Error())
	}

	err = ctx.GetStub().PutState(id, bytes);

	if (err != nil) {
		fmt.Printf("something went wrong with writing to the blockchain: %s", err.Error())
	}

	return string(bytes)

}

func (s *SmartContract) GetClient(ctx contractapi.TransactionContextInterface, id string) (string) {

	bytes, err := ctx.GetStub().GetState(id);

	if (err != nil) {
		fmt.Printf("something went wrong with reading from the blockchain: %s", err.Error())
	}

	return string(bytes)

}

func (s *SmartContract) SetBan(ctx contractapi.TransactionContextInterface, id string) (string) {
	identity := ctx.GetClientIdentity();

	if (!isAdmin(identity)) {
		errors.New("Only Administrators can Register new clients.");
	}

	bytes, err := ctx.GetStub().GetState(id)

	participant := participant.NewEmpty()

	json.Unmarshal(bytes, &participant)

	participant.Ban()

	bytes, err = json.Marshal(participant)

	if (err != nil) {
		fmt.Printf("something went wrong with encoding: %s", err.Error())
	}

	err = ctx.GetStub().PutState(id, bytes);

	return string(bytes)

}

func (s *SmartContract) SetUnBan(ctx contractapi.TransactionContextInterface, id string) (string) {
	identity := ctx.GetClientIdentity();

	if (!isAdmin(identity)) {
		errors.New("Only Administrators can Register new clients.");
	}

	bytes, err := ctx.GetStub().GetState(id);

	participant := participant.NewEmpty()

	json.Unmarshal(bytes, &participant)

	participant.UnBan()

	bytes, err = json.Marshal(participant)

	if (err != nil) {
		fmt.Printf("something went wrong with encoding: %s", err.Error())
	}

	err = ctx.GetStub().PutState(id, bytes);

	return string(bytes)

}

func isAdmin(identity cid.ClientIdentity) bool {
	id, err := identity.GetX509Certificate()
	if err != nil {
		fmt.Printf("something went wrong getting the certificate: %s", err.Error())
	}

	if id.Subject.CommonName == "admin" {
		return true
	} else {
		return false
	}
}


func main() {

	chaincode, err := contractapi.NewChaincode(new(SmartContract))

	if err != nil {
		fmt.Printf("Error creating chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting chaincode: %s", err.Error())
	}

	fmt.Print("Chaincode create successfully")
}
