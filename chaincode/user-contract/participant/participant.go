package participant

type participant struct {

	Id string
	Name string
	Role string
	Banned	bool

}

func New(id string, name string, role string) participant {
	p := participant{}
	p.Id = id
	p.Name = name
	p.Role = role
	p.Banned = false

	return p
}

func NewEmpty() participant{
	return participant{}
}


// getters
func (p participant) GetId() string{
	return p.Id
}

func (p participant) GetName() string {
	return p.Name
}

func (p participant) GetRole() string {
	return p.Role
}

func (p participant) isBanned() bool {
	return p.Banned
}

func (p participant) SetName(newName string) {
	p.Name = newName
}

func (p participant) SetRole(newRole string) {
	p.Role = newRole
}

func (p participant) Ban() {
	p.Banned = true
}

func (p participant) UnBan() {
	p.Banned = true
}