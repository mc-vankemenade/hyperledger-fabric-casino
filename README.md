# Hyperledger Fabric Casino
This project aims to build a proof of concept casino using hyperledger fabric and chaincode written in Go and javascript.

We are going to create a private blockchain for casinos. To participate in a casino, you need to be accepted first and exchange your coins to have some balance. In the casino you can play multiple games and win/lose your money (assets). We decided to go for this unusual use case because we wanted to do something out of the ordinary and not something like a Supply Chain. 

In order to support these games a user management and credit system has to be created. The user management system needs to be able to register gamblers, games, and casino’s. The credit system allows registered casino’s to issue credits to gamblers in exchange for money. These credits can then be used to play games at all of the registered casino’s.
When the user is done playing games they can cash out their winnings and change them back to money.

The different games can be played using the issued credits. The outcome of these games is registered on the blockchain as well.


---
## Stakeholders
- Casinos
- Visitors
- Game designers
- Auditors

## Use case diagram
The diagram below illustrates the process of the whole casino with its stakeholders.
<img src="./images/usecase.png" width="85%">

---

## Basic game example
the diagram below illustrates the process of this basic game where you guess a random generated number.
<img src="./images/Guessgame.png" width="50%">

---
## Getting started

### 1. Prerequisites

You need the following to continue
- [Node Version Manager](https://github.com/nvm-sh/nvm)
- [NodeJS](https://nodejs.org/en/) (version v16.14)
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Docker + Docker-compose](https://docs.docker.com/get-docker/)
- [GO](https://go.dev/learn/)
- [Postman](https://www.postman.com/downloads/)

You must have the Docker images of hyperledger fabric in order to proceed. You can install them using the instructions [here](https://hyperledger-fabric.readthedocs.io/en/latest/install.html)


### 2. Installation
Clone the Repo
```sh
git clone https://gitlab.com/mc-vankemenade/hyperledger-fabric-casino.git
```

### 3. creating the network
1. open a terminal window
2. navigate to our project folder you just cloned and go to `test-network`
3. Execute the following command to create and start the network and create the channel:

./network.sh up createChannel -ca -s couchdb```sh

```

### 4. Deploying the smart contracts
1. inside the `test-network` folder, run the following commands to deploy the different smart-contracts:

```sh
./network.sh deployCC -ccn credit-management -ccp ../chaincode/credit-management -ccl go
```

2. run the following command to deploy the User system:

```sh
./network.sh deployCC -ccn user-contract -ccp ../chaincode/user-contract/ -ccl go
```

3. run the following command to deploy the Game system:

```sh
./network.sh deployCC -ccn game-contract -ccp ../chaincode/game-contract/ -ccl go
```

4. runt eh following command to deploy the example game:

```sh
./network.sh deployCC -ccn guessnum -ccp ../chaincode/guessnum/ -ccl go
```

### 5. The Client application
In order to interact with the different smart contract, we have built a REST-based API. in the ``client/backend/application` folder you can find the following:
- **src/app.js**: The app exposes the API. It uses the Express library to expose resources
- **src/fabric/network.js**: The network.js uses the Fabric API to connect to the network
- **config.json**: This file contains relevant information for the connection
- **package.json**: contains the required libraries to run the app

A client needs some basic information to connect to the network. You will see that the file config.json is pointing to a connection profile. This file points to the test-network that we just created and uses certain parameters to connect.

**Install dependencies**

Before we begin, please install the dependencies in order to run the client. navigate to ``client/backend/application` and run the following command:
```sh
npm --logevel=error install
```

**Set env variable**

The client uses a variable called **FABRIC_PATH** that points to the hyperledger components. We have implemented these folders in our repository to make it easy. Run the following command to set this variable (under basefolder fill in your path to the cloned repository):
```sh
export FABRIC_PATH=[basefolder]/hyperledger-fabric-casino/
```

**Node version**

Make sure you use the right Node version, otherwise you will get trouble running application. Please run the following command:
```sh
nvm use v16.14
```

**Adding the first user**

The first user we are going to add is the admin user. In order to add the admin, run the following command in `client/backend/application` to enroll:
```sh
node src/enrolladmin.js
```

**Running the App**

Now we are ready to start the application. In order to run the app, please run the following command in `client/backend/application`:
```sh
node src/app.js
```
This command will start an HTTP server running on port 8080.


### 6. Using Postman

To interact with the API, you can use the Postman application. We created a few Postman collections so that you can easily import the different post and get calls. Under `postman-collections` You can find all the collections used in this project. To import, open Postman and click on import. Then select all the json collection files under `postman-collections`.
Follow the following steps to get started:

1. Create a new user for yourself, run register user
2. Add the game to the gameslist, run the Register game
3. Mint some tokens for the admin to create its adres in the ERC20 token.
4. Mint some tokens for your user aswell, change the username in the body
5. Run the Init function of the Guess game to initialize the starting prize
6. Make a Guess using you user name
7. Check your balance afterwards

---
## Folder structure:
The repository is structured in the following way:
- **`/bin`** - contains binaries of hyperledger for the test-network
- **`/chaincode`** - contains the smart contracts of this project
  - **`/user-contract`** - the user management system
  - **`/developer-contract`** - the game registry and management system
  - **`/credit-management`** - the credit management system (ERC20 Token)
  - **`/guessgame`** - a game where you guess a random number
- **`/client`** - for the client interaction
  - **`/backend`** - API that is REST based
- **`/config`** - contains configfiles of hyperledger for the test-network
- **`/images`** - contains the figures and diagrams
- **`/postman-collections`** - contains the Postman collections to easily import all the post and get calls.
- **`/test-network`** - the hyperledger Test-network to setup the network.



## Created by:
- Dries van den Beuken
- Mathijs van Kemenade



