'use strict';

const express = require('express');
const cors = require('cors');

const app = express();
app.use(express.json());
app.use(cors());

let network = require('./fabric/network.js');

let auth = require('./controllers/user.controller');

//---------------------------
// User Management functions
//---------------------------
app.post('/rest/participants', async (req, res) => {
    if(!req.body.id || !req.body.password || !req.body.newUser) {
        res.status(400).send("missing required fields");
    }

    if(auth.checkCredentials('admin', req.body.password) && req.body.id == "admin") {

        // creating the identity for the user and add it to the wallet
        let response = await network.registerUser(req.body.newUser.id, req.body.newUser.name, req.body.newUser.role);

        if (response.error) {
            res.status(400).json({ message: response.error });

        } else {
            let adminUser = await network.getAdminUser();

            let networkObj = await network.connectToNetwork(adminUser, "user-contract");

            if (networkObj.error) {
                res.status(400).json({ message: networkObj.error });
            }

            let invokeResponse = await network.createParticipant(networkObj, req.body.newUser.id, req.body.newUser.name, req.body.newUser.role);
            if (invokeResponse.error) {
                res.status(400).json({ message: invokeResponse.error });
            } else {
                auth.addPassword(req.body.newUser.id, req.body.newUser.password);
                res.setHeader('Content-Type', 'application/json');
                res.status(201).send(invokeResponse);
            }
        }
    } else {
        res.status(201).send("invalid credentials");
    }
});

app.get('/rest/participants', async (req, res) => {
    console.log(`client ${req.body.id} connected`);

    if(!req.body.id || !req.body.password) {
        res.status(400).send("missing required fields");
    }

    if(auth.checkCredentials(req.body.id, req.body.password)) {
        let networkObj = await network.connectToNetwork(req.body.id, "user-contract");

        if (networkObj.error) {
            res.status(400).json({ message: networkObj.error });
        }
    
        let invokeResponse = await network.getParticipant(networkObj, req.body.id);
    
        if (invokeResponse.error) {
            res.status(400).json({ message: invokeResponse.error });
        } else {
            res.setHeader('Content-Type', 'application/json');
            res.status(201).send(invokeResponse);
        }
    } else {
        res.status(201).send("invalid credentials");
    }
});

app.put('/rest/participants', async (req, res) => {
    console.log(`client ${req.body.id} connected`)

    if(!req.body.id || !req.body.password || !req.body.userId|| !req.body.banned) {
        res.status(400).send("missing required fields");
    }

    if(auth.checkCredentials('admin', req.body.password) && req.body.id == "admin") {
        let adminUser = await network.getAdminUser();

        let networkObj = await network.connectToNetwork(adminUser, "user-contract");

        if (networkObj.error) {
            res.status(400).json({ message: networkObj.error });
        }

        let invokeResponse;

        switch(req.body.banned) {
            case true:
                invokeResponse = await network.ban(networkObj, req.body.userId);
                break;
            
            default:
                invokeResponse = await network.unBan(networkObj, req.body.userId);
                break;
        }
        
    
        if (invokeResponse.error) {
            res.status(400).json({ message: invokeResponse.error });
        } else {
            res.setHeader('Content-Type', 'application/json');
            res.status(201).send(invokeResponse);
        }
    } else {
        res.status(201).send("invalid credentials");
    }
});

//---------------------------
// Game management functions
//---------------------------
app.post('/rest/games', async (req, res) => {
    if(!req.body.id || !req.body.password || !req.body.newGame) {
        res.status(400).send("missing required fields");
    }

    if(auth.checkCredentials('admin', req.body.password) && req.body.id == "admin") {

        let adminUser = await network.getAdminUser();

        let networkObj = await network.connectToNetwork(adminUser, "game-contract");

        if (networkObj.error) {
            res.status(400).json({ message: networkObj.error });
        }

        let invokeResponse = await network.createGame(networkObj, req.body.newGame.id, req.body.newGame.name, req.body.newGame.desc, req.body.newGame.isEnabled);
        
        if (invokeResponse.error) {
            res.status(400).json({ message: invokeResponse.error });
        } else {
            res.setHeader('Content-Type', 'application/json');
            res.status(201).send(invokeResponse);
        }
    } else {
        res.status(201).send("invalid credentials");
    }
});

app.get('/rest/games', async (req, res) => {
    console.log(`client ${req.body.id} connected`);

    if(!req.body.id || !req.body.password || !req.body.gameId) {
        res.status(400).send("missing required fields");
    }

    if(auth.checkCredentials(req.body.id, req.body.password)) {
        let networkObj = await network.connectToNetwork(req.body.id, "game-contract");

        if (networkObj.error) {
            res.status(400).json({ message: networkObj.error });
        }
    
        let invokeResponse = await network.getGame(networkObj, req.body.gameId);
    
        if (invokeResponse.error) {
            res.status(400).json({ message: invokeResponse.error });
        } else {
            res.setHeader('Content-Type', 'application/json');
            res.status(201).send(invokeResponse);
        }
    } else {
        res.status(201).send("invalid credentials");
    }
});

app.put('/rest/games', async (req, res) => {
    console.log(`client ${req.body.id} connected`)

    if(!req.body.id || !req.body.password || !req.body.gameId || !req.body.isEnabled) {
        res.status(400).send("missing required fields");
    }

    if(auth.checkCredentials('admin', req.body.password) && req.body.id == "admin") {
        let adminUser = await network.getAdminUser();

        let networkObj = await network.connectToNetwork(adminUser, "game-contract");

        if (networkObj.error) {
            res.status(400).json({ message: networkObj.error });
        }

        let invokeResponse;

        switch(req.body.isEnabled) {
            case true:
                invokeResponse = await network.enableGame(networkObj, req.body.gameId);
                break;
            
            default:
                invokeResponse = await network.disableGame(networkObj, req.body.gameId);
                break;
        }
        
    
        if (invokeResponse.error) {
            res.status(400).json({ message: invokeResponse.error });
        } else {
            res.setHeader('Content-Type', 'application/json');
            res.status(201).send(invokeResponse);
        }
    } else {
        res.status(201).send("invalid credentials");
    }
});

//---------------------
// Token use Functions
//---------------------
app.post('/rest/initledger', async (req, res) => {
    let adminUser = await network.getAdminUser();
    console.log('admin',adminUser);

    let networkObj = await network.connectToNetwork(adminUser, "guessnum");
    console.log('networkObj',networkObj);

    if (networkObj.error) {
        res.status(400).json({ message: networkObj.error });
    }

    let invokeResponse = await network.initledger(networkObj);

    if (invokeResponse.error) {
        res.status(400).json({ message: invokeResponse.error });
    } else {
        res.setHeader('Content-Type', 'application/json');
        res.status(201).send(invokeResponse);
    }
});

app.post('/rest/guessnum', async (req, res) => {
    let adminUser = await network.getAdminUser();
    console.log('admin',adminUser);

    // user connect to guessnum
    let networkObj = await network.connectToNetwork(req.body.id, "guessnum");
    console.log('networkObj',networkObj);
    if (networkObj.error) {
        res.status(400).json({ message: networkObj.error });
    }
    // user connect to token
    let networkObj2 = await network.connectToNetwork(req.body.id, "casino_token")
    if (networkObj2.error) {
        res.status(400).json({ message: networkObj.error });
    }
    // connect to token as admin
    let networkObjadmin = await network.connectToNetwork(adminUser, "casino_token")
    if (networkObjadmin.error) {
        res.status(400).json({ message: networkObj.error });
    }

    // get user account addresses
    let useraccount = await network.clientaccountid(networkObj2)

    // get guess of user
    let invokeResponse = await network.guessnum(networkObj, req.body.guess);
    if (invokeResponse.error) {
        res.status(400).json({ message: invokeResponse.error });
    } else {
        res.setHeader('Content-Type', 'application/json');
        res.status(201).send(invokeResponse);
        let response = JSON.parse(invokeResponse)
        if (response.hasOwnProperty('prize')) {
            console.log(response['prize'])
            try {
                network.transfer(networkObjadmin, useraccount, response['prize'])
            } catch(err) {
                console.log(err)
            }
            
        }
    }
});

app.post('/rest/mint', async (req, res) => {
    let adminUser = await network.getAdminUser();
    console.log('admin',adminUser);

    let networkObj = await network.connectToNetwork(req.body.id, "casino_token");
    console.log('networkObj',networkObj);

    if (networkObj.error) {
        res.status(400).json({ message: networkObj.error });
    }

    let invokeResponse = await network.mint(networkObj, req.body.amount);

    if (invokeResponse.error) {
        res.status(400).json({ message: invokeResponse.error });
    } else {
        res.setHeader('Content-Type', 'application/json');
        res.status(201).send(invokeResponse);
    }
});

app.post('/rest/burn', async (req, res) => {
    let adminUser = await network.getAdminUser();
    console.log('admin',adminUser);

    let networkObj = await network.connectToNetwork(req.body.id, "casino_token");
    console.log('networkObj',networkObj);

    if (networkObj.error) {
        res.status(400).json({ message: networkObj.error });
    }

    let invokeResponse = await network.burn(networkObj, req.body.amount);

    if (invokeResponse.error) {
        res.status(400).json({ message: invokeResponse.error });
    } else {
        res.setHeader('Content-Type', 'application/json');
        res.status(201).send(invokeResponse);
    }
});

app.post('/rest/clientaccountbalance', async (req, res) => {
    let adminUser = await network.getAdminUser();
    console.log('admin',adminUser);

    let networkObj = await network.connectToNetwork(req.body.id, "casino_token");
    console.log('networkObj',networkObj);

    if (networkObj.error) {
        res.status(400).json({ message: networkObj.error });
    }

    let invokeResponse = await network.clientaccountbalance(networkObj);

    if (invokeResponse.error) {
        res.status(400).json({ message: invokeResponse.error });
    } else {
        res.setHeader('Content-Type', 'application/json');
        res.status(201).send(invokeResponse);
    }
});

app.post('/rest/clientaccountid', async (req, res) => {
    let adminUser = await network.getAdminUser();
    console.log('admin',adminUser);

    let networkObj = await network.connectToNetwork(req.body.id, "casino_token");
    console.log('networkObj',networkObj);

    if (networkObj.error) {
        res.status(400).json({ message: networkObj.error });
    }

    let invokeResponse = await network.clientaccountid(networkObj);

    if (invokeResponse.error) {
        res.status(400).json({ message: invokeResponse.error });
    } else {
        res.setHeader('Content-Type', 'application/json');
        res.status(201).send(invokeResponse);
    }
});

app.post('/rest/totalsupply', async (req, res) => {
    let adminUser = await network.getAdminUser();
    console.log('admin',adminUser);

    let networkObj = await network.connectToNetwork(adminUser, "casino_token");
    console.log('networkObj',networkObj);

    if (networkObj.error) {
        res.status(400).json({ message: networkObj.error });
    }

    let invokeResponse = await network.totalsupply(networkObj);

    if (invokeResponse.error) {
        res.status(400).json({ message: invokeResponse.error });
    } else {
        res.setHeader('Content-Type', 'application/json');
        res.status(201).send(invokeResponse);
    }
});

app.post('/rest/receivetokens', async (req, res) => {
    let adminUser = await network.getAdminUser();

    let networkObj = await network.connectToNetwork(req.body.id, "casino_token")
    let useraccount = await network.clientaccountid(networkObj)

    let networkObjadmin = await network.connectToNetwork(adminUser, "casino_token")
    
    let invokeResponse = await network.transfer(networkObjadmin, useraccount, req.body.amount)
        if (invokeResponse.error) {
            res.status(400).json({ message: invokeResponse.error });
        } else {
            res.setHeader('Content-Type', 'application/json');
            res.status(201).send(invokeResponse);
        }
})


app.listen(8080, '0.0.0.0');
console.log('back-end ready');
