const fs = require('fs');
const path = require('path');
const listPath = path.join(process.cwd(),'/wallet/userkeys.json');

function getList() {
   return JSON.parse(fs.readFileSync(listPath, 'utf8'));

}

function setList(userList) {
    fs.writeFileSync(listPath, JSON.stringify(userList));
}


exports.checkCredentials = (userId, userPass) => {
    let userList = getList();
    //maybe store everything hashed to increase security
    if(userList[userId] == userPass) {
        return true;
    }
    return false;
}

exports.addPassword = (userId, userPass) => {
    let userList = getList();

    if(userList[userId] == undefined) {
        userList[userId] = userPass;
        console.log(userList);
        setList(userList);
        console.log(`User ${userId} added succesfully`);
    }
}

exports.updatePassword = (userId, oldPass, newPass) => {
    let userList = getList();

    if(userList[userId] == oldPass) {
        userList[userId] = newPass;
        setList(userList);
        console.log(`User ${userId} password updated succesfully`);
    }
}